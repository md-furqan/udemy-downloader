import os, sys, json

class ProcessLog:
	def __init__(self, outDir):
		self.outDir = outDir
		self.fileName = outDir + '/' + 'processLog_' + outDir + '.json'
		self.enabled = True
		if not os.path.exists(outDir):
			os.makedirs(outDir)
		if not os.path.exists(self.fileName):
			f = open(self.fileName, 'w')
			f.close()
		try:
			f = open(self.fileName, 'r')
			data = f.read()
			self.data = {}
			if len(data):
				self.data = json.loads(data)
				
		except Exception as e:
			print('Unable to load/create process log.. Exiting')
			print(e)
			sys.exit()
			self.enabled = False
			pass

	def get(self, name):
		if self.enabled:
			return name in self.data.keys()
		return False

	def put(self, newItem):
		if self.enabled:
			self.data[newItem] = True
			self.writeOutput()

	def writeOutput(self):
		try:
			f = open(self.fileName, 'w')
			f.write(json.dumps(self.data, indent=4))
			f.close()
		except Exception:
			self.enabled = False
			pass
