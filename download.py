import json, subprocess, sys, argparse
from udemy import AUTH_TOKEN, OUT_DIR, download, getVideoCurl, getCourseVideos, filterVideo, getVideoUrl
from processlog import ProcessLog

parser = argparse.ArgumentParser(description='Udemy Course Downloader')
parser.add_argument('-cid', '--courseId', type=int, help='ID of Udemy course to download', required=True)
parser.add_argument('-dir', '--outDir', type=str, help='Location of directory to download to', required=False, default=OUT_DIR)
args = parser.parse_args()

def escape(_str):
	return _str.replace('/', '.')

def main(courseId):
	courseData = getCourseVideos(courseId)
	if not courseData:
		print('Error getting course data')
		sys.exit()

	courseData = json.loads(courseData)
	lectures = [ (lecture["title"], lecture["id"]) for lecture in list(filter(filterVideo, courseData["results"])) ]

	log = ProcessLog(args.outDir)

	for idx, lecture in enumerate(lectures):
		fileName = str(idx) + ". " + escape(lecture[0]) + ".mp4"
		if log.get(fileName): # assume file is already downloaded
			continue
		# Long-running loop. No fail-safe to resume after error
		stdout = subprocess.check_output(getVideoCurl(args.courseId, lecture[1]), shell=True)
		parsedRes = json.loads(stdout)
		videoUrl = getVideoUrl(parsedRes)
		download(videoUrl, fileName, args.outDir)
		# Put the downloaded filename in log, so we download it again
		log.put(fileName)

if __name__ == "__main__":
	main(args.courseId)
