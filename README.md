# How To

Example Usage:
	python3 download.py -cid 781532 --outDir="Advanced React and Redux"

## Params: 
	*Required* Course ID (-cid or --courseId)
	*Optional* Download Directory (-cid or --outDir)
