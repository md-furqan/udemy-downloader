import os
from urllib.request import Request, urlopen

AUTH_TOKEN = "Bearer Nr0or0dTgdr6mFEco8jyS09L4HUPJyMsDHKCRbpf"
OUT_DIR = "./Downloads/"

def getVideoCurl(courseId, videoId): # Find REST alternative and remove this cURL
  curlTpl = """curl 'https://www.udemy.com/api-2.0/users/me/subscribed-courses/{courseId}/lectures/{videoId}?fields%5Basset%5D=@min,download_urls,external_url,slide_urls,status,captions,thumbnail_url,time_estimation,stream_urls&fields%5Bcourse%5D=id,is_paid,url,locale&fields%5Blecture%5D=@default,course,can_give_cc_feedback,can_see_cc_feedback_popup,download_url,can_give_transcript_feedback' -H 'authorization: {authToken}' -H 'cookie: ud_firstvisit=2018-05-11T11:34:39.351788+00:00:1fH6Jr:64d_AHQYmO9qHBmmLAa-MSmdr7k; __udmy_2_v57r=061c29fb08e14f829de2aa28fc454175; _ga=GA1.2.154753724.1526038481; _gid=GA1.2.16726328.1526038481; IR_gbd=udemy.com; IR_PI=1526038481883.47funwnsv79; _pxvid=4bee50e0-550f-11e8-936f-89858f130fb1; ki_r=; ki_s=186240%3A0.0.0.0.0; csrftoken=LVkK6YTefRKIuUvLSgrQP0PkEwNmAe0LrqTNsFlfPvjlOeE9CBeunUoaqUjgaEyv; access_token={tokenVal}; dj_session_id=t18sx6gyde0adn51d0ku5eqkwp7ok0db; client_id=bd2565cb7b0c313f5e9bae44961e8db2; IR_5420=1526038512224%7C0%7C1526038481883; __ar_v4=VH5RDLCI7NHB5GFMJDUBVW%3A20180510%3A2%7COKLCQMMNANCRNGGEOKKR5M%3A20180510%3A2%7C554CPNW4XBAX5EYKBL4HVU%3A20180510%3A2; _ga=GA1.1.154753724.1526038481; _gid=GA1.1.16726328.1526038481; intercom-lou-sehj53dd=1; eva=SlFIM0xYDm4AQRl5TFhGfAlFVm1MAAc7E0lQY19QQXgTSRhwXFAIYBMVTmNUGVd9A0AbY1RRRXQJXwlwXFFNbgtAGnFYH1luQgQJexVARH0FSwl7XFpNfEwO; ki_t=1526038489039%3B1526038489039%3B1526038518967%3B1%3B4; intercom-session-sehj53dd=N2ZENmFUajBXbExoakxrQzBvalA4ZUFHZXVsblZuSmg1UGUxd0J6c09kR0tVREhUcXZpSjNoSUp5Z243MnFzRy0tUllmc01DakVvTDkwZXVCeHdjZTF3Zz09--3c7c0e1a0c1f5c04f13563c7a5a0b97c5c0c12f5; volume=0.5; mute=0; playbackspeed=1; seen=1; _px2=eyJ1IjoiNjFjZmY4NTAtNTUwZi0xMWU4LWI4ZTAtN2Q0MGU0ODkzNDQ0IiwidiI6IjRiZWU1MGUwLTU1MGYtMTFlOC05MzZmLTg5ODU4ZjEzMGZiMSIsInQiOjE1MjYwNDE3Mzk1NDksImgiOiIzZjVhNDIzZWY0OTk1MmJhYjM0MjQ3OWJmZWUyNDc1MjM5MjVhZjZhZjdjMDU5NmNjNmQzMGVlZDZlMWMyZWQ4In0=; _px3=3f2513560767d0d8224cadf86f1a2578386a213166364f4565953e3b86a21496:CjJLaqKfKq3CuYWhjc0mAKYjojMJpJ7qLafp1IVWSjlJA4I/4ApQiCCzkY9Ovlqh+cL7IeIHRUIe7FJ6j03Uxw==:1000:6Brup/60GDF0XhSqx845+7otNkhpe4y+JH0dhQBeuhY/tOd16KBTnogdC86LnPSlCWn8EKGcFjlODAKpf6zw5k8RSYM2+u36cu9a25Oeg/X+ySlP5c8mReGH++nEtALeWDYbtwGVKknaZfMv3QbjTXV1UM5TyFrxzKHy5HKsKfw=; ud_rule_vars=eJyFjssKwjAQRX-lzFYrM3nYJN8SKDGdSFAoptGFpf9ukOLW1YX7ONwVaihXrjyNr7zkOheHZ4rCpgsaJpWMsBOLEIRJUWlFg3Zxnm-ZwXWweriHpY6FH09uOoXKvgUeBJLpUfdEHZGTykl70lIMVh8QHaKHY2vFUOq45Pc-2t2US2N97_whSmWN_BE32D5yTjrv:1fH73D:z-QSOEfjsEFFhfPFMLi1_0rLt-8' -H 'accept-encoding: gzip, deflate, br' -H 'accept-language: en-US,en;q=0.9' -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.146 Safari/537.36' -H 'x-udemy-authorization: {authToken}' -H 'accept: application/json, text/plain, */*' -H 'referer: https://www.udemy.com/master-functional-js/learn/v4/t/lecture/8444874?start=0' -H 'authority: www.udemy.com' -H 'x-requested-with: XMLHttpRequest' --compressed"""
  return curlTpl.format(
    courseId=courseId, 
    videoId=videoId, 
    authToken=AUTH_TOKEN,
    tokenVal=AUTH_TOKEN.split(' ')[1]
    )

def download(url, fileName, directory):
  if not os.path.exists(directory):
    os.makedirs(directory)
  request = Request(url)
  request.add_header("authorization", AUTH_TOKEN)
  request.add_header("x-udemy-authorization", AUTH_TOKEN)
  print('\n\nDownloading ' + fileName)
  connection = urlopen(request)
  f = open(directory + ("/" if directory[-1] != "/" else "") + fileName, 'wb')
  f.write(connection.read())
  f.close()
  print('Downloaded ' + fileName + '\n\n')

def getCourseVideos(courseId):
  request = Request("""https://www.udemy.com/api-2.0/courses/""" + str(courseId) + """/cached-subscriber-curriculum-items?fields%5Basset%5D=@min,title,filename,asset_type,external_url,length,status&fields%5Bchapter%5D=@min,description,object_index,title,sort_order&fields%5Blecture%5D=@min,object_index,asset,supplementary_assets,sort_order,is_published,is_free&fields%5Bpractice%5D=@min,object_index,title,sort_order,is_published&fields%5Bquiz%5D=@min,object_index,title,sort_order,is_published&page_size=9999""")
  request.add_header("authorization", AUTH_TOKEN)
  request.add_header("x-udemy-authorization", AUTH_TOKEN)
  connection = urlopen(request)
  f = open('./Course Log - Videos -' + str(courseId) + '.json', 'wb')
  data = connection.read()
  f.write(data)
  f.close()
  return data

def filterVideo(obj):
	return obj["_class"] == "lecture" and obj["asset"]["asset_type"] == "Video"

def getVideoUrl(data): # TODO: try/catch
	return data["asset"]["stream_urls"]["Video"][0]['file']

